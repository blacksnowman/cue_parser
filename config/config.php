<?php
/**
 * Created by PhpStorm.
 * User: amikhaylov
 * Date: 17.04.19
 * Time: 19:26
 */

return [
    'encoders' => [
        "mp3_codec"=> [
            "name" => "LAME",
            "bin" => "/usr/bin/lame",
            "extension" => "mp3",
            "output_dir" => "mp3_files",
            "default_args" => [ "-b320", "-h", "--add-id3v2", "--id3v2-only", "--quiet" ]
        ],
        "git s" => [
            "name"=> "FLAC",
            "bin" => "/usr/bin/flac",
            "extension" => "flac"
        ],
        "splitter" => [
            "name" => "SHNSPLIT",
            "bin"  => "/usr/bin/shnsplit",
            "split_format" => "%f"
        ]
    ],
    "directories" => [
        "wav_dir" => "wav_files",
        "mp3_dir" => "mp3_files",
        'tmp_dir' => "tmp"
    ]
];