<?php
/**
 * Created by PhpStorm.
 * User: amikhaylov
 * Date: 17.04.19
 * Time: 19:21
 */

namespace App\Helpers;

class ConfigReader
{
    private static $_instance = null;

    const CONFIG_FILE = "config.php";
    const CONFIG_DIR = "config";

    private $path;
    private $config_data;

    public static function getInstance(string $config_file = null)
    {
        if (self::$_instance === null) {
            self::$_instance = new self($config_file);
        }

        return self::$_instance;
    }

    private function __construct(string $config_file = null)
    {
        $this->path = "config/";

        if($config_file !== null) {
            $config_path = self::CONFIG_DIR."/".$config_file;
        } else {
            $config_path = self::CONFIG_DIR."/".self::CONFIG_FILE;
        }

        if(file_exists($config_path)) {
            $this->config_data = include_once ($config_path);
        }
    }

    public function getConfig()
    {
        return $this->config_data;
    }

    private function search(array $data = [], string $key)
    {
        $result = null;

        foreach($data as $k => $v) {
            if($k == $key) {
                return $v;
            }

            if(is_array($v)) {
                $result = $this->search($v, $key);
            }

            if($result !== null) {
                return $result;
            }
        }

        return null;
    }

    public function getElement(string $key)
    {
        return $this->search($this->config_data, $key);
    }
}