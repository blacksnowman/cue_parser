<?php
/**
 * Created by PhpStorm.
 * User: amikhaylov
 * Date: 04.10.18
 * Time: 16:32
 */

namespace App\Models;

use App\Helpers\ConfigReader;
use Exception;

class FileManager
{
    const MAX_RM_LEVEL = 3;
    const WAV_DIR = "wav";
    const MP3_DIR = "mp3";
    const TMP_DIR = "tmp";

    private $dir;

    private $wav_dir;
    private $mp3_dir;
    private $tmp_dir;

    /** @var ConfigReader $config */
    private $config;

    public function __construct(string $dir = null)
    {
        $this->dir = $dir ?? null;
        $this->loadConfig();
    }

    private function loadConfig()
    {
        $this->config = ConfigReader::getInstance();
        $config = $this->config->getElement("directories");

        $this->wav_dir = $config['wav_dir'] ?? self::WAV_DIR;
        $this->mp3_dir = $config['mp3_files'] ?? self::MP3_DIR;
        $this->tmp_dir = $config['tmp_dir'] ?? self::TMP_DIR;
    }

    public function createDirectory(string $dir)
    {
        printf("Creating directory: %s....\n", $dir);
        if(! file_exists($dir)) {
            mkdir($dir, 0775);
        } else {
            $this->removeDirectory($dir);
            mkdir($dir, 0775);
        }

        return file_exists($dir);
    }

    public function createWavDirectory()
    {
        $wav_dir = $this->getWAVdir();
        if($this->createDirectory($wav_dir)){
            return $wav_dir;
        } else {
            throw new Exception("Failed to create directory [$wav_dir]");
        }
    }


    public function createMP3Directory()
    {
        $dir = $this->getMP3Dir();
        if($this->createDirectory($dir)){
            return $dir;
        } else {
            throw new Exception("Failed to create directory [$dir]");
        }
    }

    public function removeDirectory( string $dir, int $level = 0 )
    {
        if($level > self::MAX_RM_LEVEL) {
            return false;
        }

        if( is_dir ($dir) && file_exists($dir) ) {
            if ($handle = opendir($dir)) {
                while (false !== ($entry = readdir($handle))) {
                    if (($entry!=".")&&($entry!="..")){
                        if (is_dir($dir . "/" . $entry)) {
                            $this->removeDirectory($dir . "/" . $entry, ($level+1) );
                        }
                    }
                }
                closedir($handle);
            }

            $mask = $dir . "/*";
            array_map("unlink", glob($mask));
            return rmdir($dir);
        } else {
            return false;
        }
    }

    public function removeWavDir()
    {
        $wav_dir = $this->getWAVdir();
        return $this->removeDirectory( $wav_dir );
    }

    public function removeTmpDir()
    {
        $dir = $this->getTmpDir();
        return $this->removeDirectory( $dir );
    }

    public function removeTemporary()
    {
        $this->removeWavDir();
        $this->removeTmpDir();
    }

    public function getWAVdir()
    {
        return $this->dir."/".$this->wav_dir;
    }

    public function getMP3Dir()
    {
        return $this->dir."/".$this->mp3_dir;
    }

    public function getTmpDir()
    {
        return $this->dir."/".$this->tmp_dir;
    }
}