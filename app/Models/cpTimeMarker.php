<?php
/**
 * Created by PhpStorm.
 * User: prog08
 * Date: 30.08.17
 * Time: 19:37
 */

namespace App\Models;

class cpTimeMarker
{
    private $startTime = null;
    private $endTime = null;
    public $timeValue = 0;
    public $memoryUsage = null;
    public $name = null;

    public function __construct($name,$start,$end){
        $this->name = $name ?? null;
        $this->startTime = $start ?? null;
        $this->endTime = $end ?? null;
        $this->calcDuration();
        $this->getMemory();
    }

    private function calcDuration(){
        if(!is_null($this->startTime) AND !is_null($this->endTime)) {
            $this->timeValue = $this->endTime - $this->startTime;
            return $this->timeValue;
        } else {
            return null;
        }
    }

    public function getMemory(){
        $this->memoryUsage = memory_get_peak_usage()/(1024*1024);
    }

    public function getDuration(){
        return $this->timeValue;
    }

    public function __toString() {
        return (string) $this->timeValue;
    }


}