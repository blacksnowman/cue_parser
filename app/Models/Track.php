<?php
/**
 * Created by PhpStorm.
 * User: amikhaylov
 * Date: 23.05.18
 * Time: 14:47
 */

namespace App\Models;

class Track
{
    public $number;
    public $type;
    public $title;
    public $performer;
    public $index = [];
    public $file_id;

    public function __construct( array $data = null ) {
        if( $data !==  null) {
            $this->number = $data['NUMBER'];
            $this->file_id = $data['FILE'];
            $this->type = $data['TYPE'];
            $this->title = $data['TITLE'];
            $this->performer = $data['PERFORMER'] ?? null;
            $this->setIndex( $data['INDEX']);
        }
    }

    public function getTitle() {
        return $this->title;
    }

    public function getTrackNumber( string $mode = null ) {
        if($mode == "zero") {
            return sprintf("%02d", $this->number);
        } else {
            return $this->number;
        }
    }

    public function getFormattedName() {
        return sprintf("%02d - %s", $this->number, $this->title);
    }

    private function setIndex( array $data = null ){
        if($data === null) {
            return null;
        }

        foreach ($data as $i=>$datum) {
            $this->index[$i] = new CueIndex( $datum );
        }
    }
}