<?php
/**
 * Created by PhpStorm.
 * User: amikhaylov
 * Date: 23.05.18
 * Time: 15:11
 */

namespace App\Models;

class CueIndex
{
    private $minute;
    private $second;
    private $micro;

    public function __construct( string $time ) {

        $time_data = explode(":", $time);

        $this->minute = (int) $time_data[0];
        $this->second = (int) $time_data[1];
        $this->micro = $time_data[2] ?? null;

    }

    public function getIndex(){
        return sprintf("%02d:%02d.%03d",
            $this->minute,
            $this->second,
            $this->micro ?? 0
        );
    }
}