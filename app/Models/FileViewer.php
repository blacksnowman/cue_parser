<?php
/**
 * Created by PhpStorm.
 * User: amikhaylov
 * Date: 14.06.18
 * Time: 14:14
 */

namespace App\Models;

use Exception;

class FileViewer
{
    private $dir;
    private $files = [];
    private $format_list = [];

    const AUDIO_FORMATS_EXT = [
      "mp3","flac","wav","ogg","ape"
    ];

    const PLAYLIST_EXT = [
      "cue","m3u","m3u8"
    ];

    public function __construct(string $dir = null)
    {
        if($dir !== null){
            $this->dir = $dir;
        }

        $this->format_list = array_merge(self::AUDIO_FORMATS_EXT, self::PLAYLIST_EXT);
    }

    public function getDirectory() {
        return $this->dir;
    }

    public function getAllFiles(): array {
        return $this->files;
    }

    public function getFilesByFormat(string $format):array {
        return $this->files[$format] ?? [];
    }

    public function getCueFiles(): array
    {
        $files = $this->getFilesByFormat("cue");

        return $files;
    }

    public function scanDirectory() {
        if ($this->dir === null) {
            throw new Exception("Directory is not defined");
        }

        $files = [];
        if ($handle = opendir($this->dir)) {
            while (false !== ($entry = readdir($handle))) {
                if(preg_match("#(.+)\.(.+)#", $entry, $result)) {
                    $file_name = $result[1];
                    $file_format = $result[2];
                    if($this->checkFileFormat($file_format)) {
                        $files[$file_format][] = $entry;
                    }
                }
            }
            closedir($handle);
        }

        $this->files = $files;
        return $this->files;
    }

    private function checkFileFormat(string $format) {
        return in_array($format, $this->format_list);
    }

}