<?php

namespace App\Models;

use Exception;

class CueParser 
{
	private $dir;

	private $cue_files = [];
	private $titles = [];
	private $albums = [];

	const LINE_BUFFER = 1024;
    const TMP_DIR = "tmp";

	const REM = [
		"DISCID",
		"GENRE",
		"DATE",
		"COMMENT"
	];

	public function __construct(string $dir = null) {
		if($dir !== null) {
			if(is_dir($dir)) {
				$this->dir = $dir;
			}
		}
	}

	public function getCueFiles() {
		return $this->cue_files;
	}

	public function setCueFiles(array $files) {
	    $this->cue_files = $files;
    }

	public function getDir(){
	    return $this->dir;
    }

    public function getAlbums(){
	    return $this->albums;
    }

    public function hasCue() {
	    if(count($this->cue_files) > 0) {
	        return true;
        } else {
	        return false;
        }
    }

	public function parseAllFiles()
    {
	    if(! $this->hasCue()) {
	        throw new Exception ("Has no CUE files");
        }

        foreach($this->cue_files as $cue_file) {
	        $this->iconvFile($cue_file);

            $data = $this->parseCueFile( $cue_file );
            $data['CUE_FILE'] = $cue_file;

            $album = new CueAlbum( $data );
            $split_cue_files = $this->splitCUE( $album );
            if ( $split_cue_files !== null ) {
                $album->setSplitCueFiles( $split_cue_files );
            }

            $this->albums[] = $album;

            $cue_file_path = $this->dir."/".$cue_file;
            $this->createCueFile($cue_file_path, $album);
        }

        return $this->albums;
    }

    public function iconvFile(string $file)
    {
        $file = $this->dir."/".$file;
        $cmd = "iconv -f cp1251 -t utf8 \"$file\" -o \"$file\"";
        $res = exec($cmd, $output, $result);
    }

	public function parseCueFile( string $cue_file )
    {
        $file_path = $this->dir."/".$cue_file;
        if(! file_exists($file_path)) {
            return null;
        }

        $handle = fopen($file_path, 'r');
        if ($handle === false) {
           return false;
        }

        $cue_data = [];
        while (!feof($handle)) {
            $buffer = fgets($handle, self::LINE_BUFFER);
            $this->parseCueLine( $cue_data, $buffer);
        }

        fclose($handle);
        return $cue_data;
	}

	private function createCueFile(string $cue_file_path, CueAlbum $album) {
	    $path_info = pathinfo($cue_file_path);
	    $file_name = $path_info['filename']." (alt)";
        $handle = fopen($cue_file_path, "w");

        if($handle == false) {
            throw new Exception("Can't create file $cue_file_path");
        }

        foreach($album->getRemAttributes() as $key => $attribute) {
            if($attribute !== null ) {
                fwrite($handle, sprintf("REM %s \"%s\"\n", $key, $attribute));
            }
        }

        fwrite( $handle, "PERFORMER \"$album->performer\"\n");
        fwrite( $handle, "TITLE \"$album->title\"\n");
        fwrite( $handle, "FILE \"".$album->audio_files[0]."\" WAVE\n");

        /** @var Track $track */
        foreach($album->getTrackList() as $track) {
            fwrite( $handle, sprintf("  TRACK %02d AUDIO\n", $track->getTrackNumber()));
            fwrite( $handle, sprintf("    TITLE \"%s\"\n", $track->title));
            fwrite( $handle, sprintf("    PERFORMER \"%s\"\n", $track->performer ?? $album->performer));

            /**
             * @var Index $index
             */
            foreach($track->index as $i => $index) {
                fwrite( $handle, sprintf("    INDEX %02d %s\n", $i, $index->getIndex() ));
            }
        }

        # $tmp_cue_files[] = $cue_file_name;
        fclose($handle);
	}

	private function splitCUE( CueAlbum $album )
    {
        if(count($album->getAudioFiles()) == 1) {
            return null;
        }

        $tmp_dir = $this->dir."/tmp";
        if(! file_exists($tmp_dir)) {
            if (! mkdir($tmp_dir, 0775)) {
                throw new Exception("Can't create directory $tmp_dir");
            }
        }

        $tmp_cue_files = [];

        foreach ($album->getAudioFiles() as $file_id => $file_name) {
            $cue_file_name = sprintf("%s - %s - Side %02d.cue", $album->performer, $album->title, ($file_id+1) );
            $cue_file_path = $tmp_dir."/".$cue_file_name;
            $handle = fopen($cue_file_path, "w");

            if($handle == false) {
                throw new Exception("Can't create file $cue_file_path");
            }

            foreach($album->getRemAttributes() as $key => $attribute) {
                if($attribute !== null ) {
                    fwrite($handle, sprintf("REM %s \"%s\"\n", $key, $attribute));
                }
            }

            fwrite( $handle, "PERFORMER \"$album->performer\"\n");
            fwrite( $handle, "TITLE \"$album->title\"\n");
            fwrite( $handle, "FILE \"$file_name\" WAVE\n");

            /** @var Track $track */
            foreach($album->getTracksByFileId($file_id) as $track) {
                fwrite( $handle, sprintf("  TRACK %02d AUDIO\n", $track->getTrackNumber()));
                fwrite( $handle, sprintf("    TITLE \"%s\"\n", $track->title));
                fwrite( $handle, sprintf("    PERFORMER \"%s\"\n", $track->performer ?? $album->performer));

                /**
                 * @var Index $index
                 */
                foreach($track->index as $i => $index) {
                    fwrite( $handle, sprintf("    INDEX %02d %s\n", $i, $index->getIndex() ));
                }
            }

            $tmp_cue_files[] = $cue_file_name;
            fclose($handle);
        }

        return $tmp_cue_files;
    }

    private function file_write ( $handle, $line ){
        return fwrite( $handle, $line."\n");
    }

	private function parseCueLine( array &$cue_data, string $line ) {
	    $counter = $cue_data['TRACK_COUNTER'] ?? 0;

		if(preg_match("#REM\s(\S+)\s(.+)#", $line, $res)) {
			$param = $res[1] ?? null;
			$value = $res[2] ?? null;

			if(in_array($param, self::REM)) {
				$cue_data[$param] = str_replace(["\"","''"], ['',''], trim($value));
			}
		}

		if(preg_match("#^(PERFORMER|TITLE)\s(.+)$#", $line, $res)) {
            $param = $res[1];
            $value = $res[2];
		    $cue_data[$param] = str_replace(["\"","''"], ['',''], trim($value));
		}

        if(preg_match("#^(FILE)\s+\"([^\"]+)\"#", $line, $res)) {
            $param = $res[1];
            $value = $res[2];
            $cue_data[$param][] = trim($value);
        }

        if(preg_match("#(TRACK)\s+(\d+)\s+(AUDIO)#", $line, $res)) {
            $value = (int) trim($res[2]);
            $track_type = $res[3];
            $cue_data['TRACK_COUNTER'] = $value;
            $counter = $cue_data['TRACK_COUNTER'];

            $cue_data['TRACKS'][$counter-1]['NUMBER'] = $counter;
            $cue_data['TRACKS'][$counter-1]['TYPE'] = $track_type;

            $last_file_id = count($cue_data['FILE'])-1;
            $cue_data['TRACKS'][$counter-1]['FILE'] = $last_file_id;

        }

        if($counter > 0) {
            if (preg_match("#(PERFORMER|TITLE)\s(.+)$#", $line, $res)) {

                $param = $res[1];
                $value = trim($res[2]);
                $value = str_replace(["\"","''"], ['',''], $value);

                if($param == 'TITLE') {
                    if(in_array($value, $this->titles)) {
                        $value = $value." (alt)";
                    }
                }

                $this->titles[] = $value;

                $cue_data['TRACKS'][$counter-1][$param] = $value;
            }

            if (preg_match("#(INDEX)\s+(\d+)\s+(\d{2}:\d{2}:\d{2}|\d{2}:\d{2})#", $line, $res)) {
                $param = $res[1];
                $index = (int) trim($res[2]);
                $time = trim($res[3]);

                $cue_data['TRACKS'][$counter-1][$param][$index] = $time;
            }
        }
	}
}
?>