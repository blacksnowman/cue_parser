<?php
/**
 * Created by PhpStorm.
 * User: amikhaylov
 * Date: 23.05.18
 * Time: 17:01
 */

namespace App\Models;

use App\Helpers\ConfigReader;
use Exception;

class CueSplitter
{
    private $split = false;
    private $albums = [];
    private $dir;

    const SPLIT_TOOL = "/usr/bin/shnsplit";
    const SPLIT_FORMAT = "%t";

    const MAX_RM_LEVEL = 3;

    /** @var ConfigReader $config */
    private $config;
    private $bin;
    private $split_format;

    public function __construct( string $dir, array $albums = [] )
    {
        $this->loadConfig();

        $this->dir = $dir;
        $this->albums = $albums;

        $this->file_viewer = new FileViewer($dir);
        $this->file_manager = new FileManager($dir);
    }

    private function loadConfig()
    {
        $this->config = ConfigReader::getInstance();
        $config = $this->config->getElement("splitter");

        $this->bin = $config['bin'] ?? self::SPLIT_TOOL;
        $this->split_format = $config['split_format'] ?? self::SPLIT_FORMAT;
    }

    public function splitAllCue()
    {
        $wav_dir = $this->file_manager->createWavDirectory();
        $tmp_dir = $this->file_manager->getTmpDir();

        /** @var CueAlbum $album */
        foreach($this->albums as $album) {
            if($album->hasSplitCueFiles()) {
                $audio_files = $album->getAudioFiles();

                foreach ($album->getSplitCueFiles() as $i => $cue_file) {
                    $cue_file = $tmp_dir."/".$cue_file;
                    $audio_file = $this->dir."/".$audio_files[$i];
                    $result = $this->splitCue( $cue_file, $audio_file );
                }
            } else {
                $cue_file = $this->dir."/".$album->getCueFile();
                $audio_file = $this->dir."/".$album->getAudioFile();
                $result = $this->splitCue( $cue_file, $audio_file );
            }

            $pregap_file_name = $wav_dir."/pregap.wav";
            if(! file_exists($cue_file)) {
                unlink($pregap_file_name);
            }
        }
        $this->split = true;
    }

    public function splitCue(
        string $cue_file,
        string $audio_file,
        string $format = self::SPLIT_FORMAT
    ) {
        $wav_dir = $this->file_manager->getWAVdir();

        if(! file_exists($cue_file)) {
            throw new Exception(sprintf("%s doesn't exist\n", $cue_file));
        }
        //-o "wav flac -d -F %f"
        $splitter = self::SPLIT_TOOL;
        $command = "$splitter -t \"$format\" -O always -q -f \"$cue_file\" \"$audio_file\" -d \"$wav_dir\"";

        $res = exec($command, $output, $result);
        return $result;
    }

    public function convertAllAlbumsToMP3() {
        if(count($this->albums) == 0) {
            throw new Exception("Has no albums");
        }

        foreach($this->albums as $album) {
            $this->convertAlbumToMP3( $album );
        }
    }

    private function cleanFileName( string $name )
    {
        return str_replace(["/",'\\'], ['-',' '], $name);
    }

//    public function convertAlbumToMP3( CueAlbum $album ) {
//        $mp3_dir = $this->file_manager->getMP3Dir();
//
//        if(! file_exists($mp3_dir)) {
//            mkdir($mp3_dir, 0775);
//        } else {
//            $this->file_manager->removeDirectory( $mp3_dir );
//            mkdir($mp3_dir, 0775);
//        }
//
//        $pids = [];
//        if($this->mode == 0) {
//            /** @var Track $track */
//            foreach ($album->getTrackList() as $track) {
//                $pid = pcntl_fork();
//                if ($pid == -1) {
//                    die('Failed to fork\n');
//                } else {
//                    if ($pid) {
//                        $pids[] = $pid;
//                    } else {
//                        $this->convertTrackToMP3($album, $track);
//                        exit(0);
//                    }
//                }
//            }
//
//            foreach ($pids as $child) {
//                pcntl_waitpid($child, $status);
//            }
//        } else {
//            foreach ($album->getTrackList() as $track) {
//                $this->convertTrackToMP3($album, $track);
//            }
//        }
//
//        printf("Done!\n");
//    }

//    private function convertTrackToMP3( CueAlbum $album, Track $track )
//    {
//        $lame = self::MP3_ENCODER;
//        $wav_file = $this->getWAVdir()."/".$this->cleanFileName($track->getTitle()).".wav";
//        $track_name = $this->cleanFileName($track->getFormattedName());
//
//        $mp3_file = $this->getMP3Dir()."/".$track_name.".mp3";
//
//        if(! file_exists($wav_file)) {
//            throw new Exception(sprintf("File doesn't exists: %s\n", $wav_file));
//        }
//
//        $args = $this->getLameTags( $album, $track );
//        printf("Converting track: %s .... \n", $track->getFormattedName());
//
//        if($this->mode == 0) {
//            $args_list = array_merge( $args, [ $wav_file, $mp3_file ] );
//            pcntl_exec($lame, $args_list);
//            $err = pcntl_get_last_error ();
//            if( $err !== null ) {
//                print(pcntl_strerror($err));
//            }
//        } else {
//            $args = join(" ",$args);
//            $command = "$lame $args \"$wav_file\" \"$mp3_file\";";
//
//            exec($command);
//        }
//
//        print("Finished\n");
//    }


}