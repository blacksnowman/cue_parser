<?php
/**
 * Created by PhpStorm.
 * User: amikhaylov
 * Date: 04.10.18
 * Time: 16:54
 */

namespace App\Models\Encoders;

use App\Helpers\ConfigReader;
use Exception;

class MP3 implements EncoderInterface
{
    const EXT = "mp3";
    const ENCODER = "/usr/bin/lame";
    const WAV_EXT = "wav";
    const DEFAULT_ARGS = [ "-b320", "-h", "--add-id3v2", "--id3v2-only", "--quiet" ];
    const OUTPUT_DIR = "mp3";

    private $version;
    private $bin;
    private $output_dir;
    private $default_args;
    private $extension;

    /** @var ConfigReader $config */
    private $config;

    public function __construct(string $dir = null)
    {
        $this->loadConfig();

        if(!file_exists($this->bin)) {
            throw new Exception("MP3 encoder [".$this->bin."] not found");
        }

        $this->checkVersion();
    }

    private function loadConfig()
    {
        $this->config = ConfigReader::getInstance();
        $config = $this->config->getElement("mp3_codec");

        $this->output_dir = $config['output_dir'] ?? self::OUTPUT_DIR;
        $this->default_args = $config['default_args'] ?? self::DEFAULT_ARGS;
        $this->bin = $config['bin'] ?? self::ENCODER;
        $this->extension = $config['extension'] ?? self::EXT;
    }

    private function checkVersion()
    {
        $bin = self::ENCODER;
        $cmd = "$bin -v";

        $res = exec($cmd, $output, $result);
        foreach($output as $line) {
            if (preg_match("#LAME\s(\S+)\sversion\s(\S+)#", $line, $data)) {
                $this->version = $data[2];
            }
        }
    }

    public function encode(
        string $input_file,
        string $output_file,
	    array $args = null,
        bool $verbose = false,
        bool $pnctl = false
    ) {
        $encoder = $this->bin;

        if($args !== null) {
            $args = $this->getLameTags($args);
        } else {
            $args = $this->default_args;
        }

        if (!file_exists($input_file)) {
            throw new Exception("File $input_file not found");
	    }

        if($pnctl === true ) {
            $args_list = array_merge( $args, [ $input_file, $output_file ] );
            pcntl_exec($encoder, $args_list);
        } else {
            $args = $this->getStringArgs($args);
            echo $args."\n";
            $command = "$encoder $args \"$input_file\" \"$output_file\";";
            exec($command);
        }
    }

    private function getStringArgs(array $args = null)
    {
        if($args === null) {
            return "";
        }

        if (count($args) > 0) {
            $args = join(" ", $args);
        } else {
            $args = "";
        }

        return $args;
    }

    private function getLameTags( array $params )
    {
        $args = [
            "ty" => $params['date'] ?? null,
            "tg" => $params['genre'] ?? null,
            "tc" => $params['comment'] ?? null,
            "ta" => $params['artist'] ?? null,
            "tl" => $params['name'] ?? null,
            "tn" => $params['track_number'] ?? null,
            "tt" => $params['title'] ?? null,
        ];

        $tmp = [];
        foreach($args as $key=>$arg) {
            if($arg === null) {
                continue;
            }

            $tmp[] = "--$key";
            $tmp[] = "\"$arg\"";
        }

        $params = $this->default_args; //[ "-b320", "-h", "--add-id3v2", "--id3v2-only", "--quiet" ];
        return array_merge( $params ,$tmp );
    }

    public function decode(
        string $input_file,
        string $output_file,
	    array $args = [],
        bool $verbose = false
    ) {
        // TODO: Implement decode() method.
    }

    public function getVersion()
    {
        if($this->version === null) {
            $this->checkVersion();
        }
        return $this->version;
    }

    public function getExtension()
    {
        return self::EXT;
    }

    public function getExecutable()
    {
        return $this->bin;
    }
}