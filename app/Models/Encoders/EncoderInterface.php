<?php
/**
 * Created by PhpStorm.
 * User: amikhaylov
 * Date: 04.10.18
 * Time: 16:52
 */

namespace App\Models\Encoders;

interface EncoderInterface
{
    public function encode(string $input_file, string $output_file,
        array $args = [], bool $verbose = false, bool $pnctl = false);
    public function decode(string $input_file, string $output_file, array $args = [], bool $verbose = false);
    public function getVersion();
    public function getExtension();
    public function getExecutable();
}