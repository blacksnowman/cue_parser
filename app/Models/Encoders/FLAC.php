<?php
/**
 * Created by PhpStorm.
 * User: amikhaylov
 * Date: 04.10.18
 * Time: 16:07
 */

namespace App\Models\Encoders;

use bluemoehre\Flac as MetaFlac;
use App\Helpers\ConfigReader;
use Composer\Config;
use Exception;

class FLAC implements EncoderInterface
{
    const EXT = "flac";
    const ENCODER = "/usr/bin/flac";
    const WAV_EXT = "wav";
    const TOTALLY_SILENT = "--totally-silent";

    private $version;

    /** @var ConfigReader $config */
    private $config;

    private $output_dir;
    private $verbose = false;
    private $bin;

    /** @var MetaFlac $meta */
    private $meta;

    public function __construct(string $dir = null)
    {
        $this->loadConfig();
        $this->output_dir = $dir ?? null;

        if(!file_exists($this->bin)) {
            throw new Exception("FLAC encoder [".$this->bin."] not found");
        }

        $this->checkVersion();
    }

    private function loadConfig()
    {
        $this->config = ConfigReader::getInstance();
        $config = $this->config->getElement("flac_codec");

        $this->output_dir = $config['output_dir'] ?? self::WAV_EXT;
        # $this->default_args = $config['default_args'] ?? self::DEFAULT_ARGS;
        $this->bin = $config['bin'] ?? self::ENCODER;
        $this->extension = $config['extension'] ?? self::EXT;
    }

    public function setVerboseOn()
    {
        $this->verbose = true;
    }

    public function setOutputDirectory(string $dir)
    {
        $this->output_dir = $dir;
    }

    public function encode(
        string $input_file,
        string $output_file,
	    array $args = null,
        bool $verbose = false,
        bool $pnctl = false
    ) {
        // TODO: Implement encode() method.
    }

    public function decode(
        string $input_file,
        string $output_file,
	    array $args = [],
        bool $verbose = false
    ) {
        $encoder = $this->bin;
	
        if(! file_exists($input_file)) {
            throw new Exception("File $input_file not found");
        }

        $verbose = ($this->verbose === false) ? self::TOTALLY_SILENT : "";
        $command = "$encoder $verbose -o \"$output_file\" -d -F \"$input_file\"";
    
        if($verbose === true) {
            echo $command."\n";
        }

        $res = exec($command, $output, $result);

        if(! file_exists($output_file)) {
            return false;
        } else {
            return true;
        }
    }

    public function getMeta(string $file_path): MetaFlac
    {
        $meta = new MetaFlac($file_path);
        return $meta;
    }

    private function checkVersion()
    {
        $bin = $this->bin;
        $cmd = "$bin -v";

        $res = exec($cmd, $output, $result);
        if(preg_match("#flac\s(\S+)#", array_shift($output), $data)){
            $this->version = $data[1];
        }
    }

    public function getVersion()
    {
        if($this->version === null) {
            $this->checkVersion();
        }
        return $this->version;
    }

    public function getExtension()
    {
        return self::EXT;
    }

    public function getExecutable()
    {
        return $this->bin;
    }
}