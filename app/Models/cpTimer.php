<?php
/**
 * Created by PhpStorm.
 * User: prog08
 * Date: 30.08.17
 * Time: 13:18
 */

namespace App\Models;

class cpTimer
{
    private $startTime = null;
    private $endTime = null;

    private $timerValue = 0;
    private $timeMarkers = [];
    private $precision = 6;

    private $availableUnits = ['sec','micro'];
    private $units = "sec";

    public function __construct($units, $startTime = null) {
        if(!is_null($startTime) AND is_numeric($startTime)){
            $this->startTime = $startTime;
        }

        if(in_array($units,$this->availableUnits)) {
            $this->units = $units;
        }
    }

    /**
     * Start timer
     *
     * @return bool
     */
    public function start(){
        $this->startTime = microtime(true);
        return true;
    }

    private function addTimeMarker($name = null){
        if(!is_null($name)) {
            $markerName = $name;
        } else {
            $markerCounter = count($this->timeMarkers)+1;
            $markerName = sprintf("marker_%d",$markerCounter);
        }
        $marker = new cpTimeMarker($markerName,$this->startTime,$this->endTime);
        $this->timeMarkers[] = $marker;
        return $marker;
    }

    /**
     * Stop timer
     *
     * @return bool|float
     */
    public function stop($name = null){
        if(!is_null($this->startTime)) {
            $this->endTime = microtime(true);
            $marker = $this->addTimeMarker($name);
            $currentTimer = $marker->getDuration();
            return $this->roundTimeValue($currentTimer);
        } else {
            return false;
        }
    }


    /**
     * Get full time
     *
     * @return float
     */
    public function getFullTime(){
        return $this->roundTimeValue($this->timerValue);
    }


    /**
     * Get all time markers or null
     *
     * @return array|null
     */
    public function getAllMarkers(){
        if(count($this->timeMarkers)>0) {
            return $this->timeMarkers;
        } else {
            return null;
        }
    }

    public function printAllMarkers(){
        /** @var cpTimeMarker $marker */
        foreach($this->timeMarkers as $marker){
            printf("marker: %s, time: %02f\n",$marker->name, $marker->getDuration());
        }
    }

    private function roundTimeValue($timer){
       return round($timer,$this->precision);
    }

}