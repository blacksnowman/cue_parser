<?php
/**
 * Created by PhpStorm.
 * User: amikhaylov
 * Date: 18.04.19
 * Time: 15:44
 */

namespace App\Models;

use Exception;
use App\Models\Encoders\EncoderInterface;

class AlbumConverter
{
    /** @var EncoderInterface $encoder */
    private $encoder;

    /** @var FileManager $file_manager */
    private $file_manager;

    private $mode = 0;
    private $verbose = false;

    public function __construct( FileManager $manager, EncoderInterface $encoder )
    {
        $this->file_manager = $manager;
        $this->encoder = $encoder;
    }

    public function setVerboseOn()
    {
        $this->verbose = true;
    }

    private function cleanFileName( string $name )
    {
        return str_replace(["/",'\\'], ['-',' '], $name);
    }

    private function convertTrackToMP3( CueAlbum $album, Track $track )
    {
        $wav_file = $this->file_manager->getWAVdir()."/".$this->cleanFileName($track->getTitle()).".wav";
        $track_name = $this->cleanFileName($track->getFormattedName());

        $mp3_file = $this->file_manager->getMP3Dir()."/".$track_name.".mp3";

        if(! file_exists($wav_file)) {
            throw new Exception(sprintf("File doesn't exists: %s\n", $wav_file));
        }

        ///$args = $this->getLameTags( $album, $track );
        $track_params = $this->getTrackParams($album, $track);
        printf("Converting track: %s .... \n", $track->getFormattedName());

        if($this->mode == 0) {
            $this->encoder->encode($wav_file, $mp3_file, $track_params, $this->verbose, true );
            $err = pcntl_get_last_error ();
            if( $err !== null ) {
                print(pcntl_strerror($err));
            }
        } else {
            $this->encoder->encode($wav_file, $mp3_file, $track_params, $this->verbose, false );
        }

        print("Finished\n");
    }

    private function getTrackParams( CueAlbum $album, Track $track): array
    {
        $params = [];
        $params['date'] = $album->date ?? null;
        $params['genre'] = $album->genre ?? null;
        $params['comment'] = $album->comment ?? null;
        $params['artist'] = $album->getArtist() ?? null;
        $params['name'] = $album->getName() ?? null;
        $params['track_number'] = $track->getTrackNumber() ?? null;
        $params['title'] = $track->title ?? null;

        return $params;
    }

    public function convertAlbumToMP3( CueAlbum $album )
    {
        $mp3_dir = $this->file_manager->createMP3Directory();

        $pids = [];
        if($this->mode == 0) {
            /** @var Track $track */
            foreach ($album->getTrackList() as $track) {
                $pid = pcntl_fork();
                if ($pid == -1) {
                    die('Failed to fork\n');
                } else {
                    if ($pid) {
                        $pids[] = $pid;
                    } else {
                        $this->convertTrackToMP3($album, $track);
                        exit(0);
                    }
                }
            }

            foreach ($pids as $child) {
                pcntl_waitpid($child, $status);
            }
        } else {
            foreach ($album->getTrackList() as $track) {
                $this->convertTrackToMP3($album, $track);
            }
        }

        printf("Done!\n");
    }

    public function convertAllAlbumsToMP3(array $albums = []) {
        if(count($albums) == 0) {
            throw new Exception("Has no albums");
        }

        foreach($albums as $album) {
            $this->convertAlbumToMP3( $album );
        }
    }
}