<?php
/**
 * Created by PhpStorm.
 * User: amikhaylov
 * Date: 18.04.19
 * Time: 17:25
 */

namespace App\Models;


class Forker
{
    private $pids = [];

    public function __construct()
    {

    }

    public function fork(array $callback_data, array $arguments)
    {
        $pid = pcntl_fork();
        if ($pid == -1) {
            die('Failed to fork\n');
        } else {
            if ($pid) {
                $this->pids[] = $pid;
            } else {
                call_user_func_array($callback_data, $arguments);
                $err = pcntl_get_last_error ();
                if( $err !== null ) {
                    print(pcntl_strerror($err));
                }
                exit(0);
            }
        }
    }

    public function waitpid()
    {
        foreach($this->pids as $pid) {
            pcntl_waitpid($pid, $status);
        }
    }
}