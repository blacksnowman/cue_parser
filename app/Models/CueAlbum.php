<?php

namespace App\Models;

class CueAlbum 
{
	public $genre;
	public $date;
	public $disc_id;
	public $comment;
	public $performer;
	public $title;
	public $audio_files = [];

	public $cue_file;
    private $split_cue_files = [];

	private $tracks = [];
	private $track_counter = 0;

	public function __construct( array $data ) {
        $this->genre = $data['GENRE'] ?? null;
        $this->date = $data['DATE'] ?? null;
        $this->disc_id = $data['DISCID'] ?? null;
        $this->performer = $data['PERFORMER'] ;
        $this->comment = $data['COMMENT'] ?? null;
        $this->title = $data['TITLE'];
        $this->audio_files = $data['FILE'];
        $this->cue_file = $data['CUE_FILE'];

        $this->setTracks( $data['TRACKS'] );
        $this->track_counter = $data['TRACK_COUNTER'] ?? count($this->tracks);
    }

    public function getTracksByFileId(int $file_id) {
	    $result = [];
	    /** @var Track $track */
        foreach($this->tracks as $track) {
            if($track->file_id == $file_id ) {
                $result[] = $track;
            }
        }

        return $result;
    }

    public function getAudioFiles() {
        return $this->audio_files;
    }

    public function getAudioFile() {
	    return $this->audio_files[0];
    }

    public function getRemAttributes() {
	    return [
	        'GENRE' => $this->genre,
            'DATE' => $this->date,
            'COMMENT' => $this->comment,
            'DISCID' => $this->disc_id
        ];
    }

    private function setTracks( array $tracks ) {
	    if ($tracks === null) {
	        return null;
        }

	    foreach($tracks as $track){
	        $this->tracks[] = new Track( $track );
        }
    }

    public function setSplitCueFiles( array $files ) {
	    $this->split_cue_files = $files ?? [];
    }

    public function getSplitCueFiles() {
	    return $this->split_cue_files;
    }

    public function hasSplitCueFiles(): bool {
	    if(count($this->split_cue_files) > 0){
	        return true;
        } else {
	        return false;
        }
    }

    public function getArtist(){
	    return $this->performer;
    }

    public function getName(){
	    return $this->title;
    }

    public function getCueFile() {
		return $this->cue_file;
	}

	public function getTrackList() {
		return $this->tracks;
	}

	public function addTrack( string $track_name ) {
		$this->tracks[] = $track_name;
	}

	public function modifyTrack(int $id, Track $track){
	    $this->tracks[$id] = $track;
    }
}
?>