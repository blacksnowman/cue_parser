<?php
/**
 * Created by PhpStorm.
 * User: amikhaylov
 * Date: 04.10.18
 * Time: 15:30
 */

namespace App\UseCase;

use getID3;
use App\Models\Forker;
use Exception;
use App\Models\FileViewer;
use App\Models\FileManager;
use App\Models\Encoders\FLAC;
use App\Models\Encoders\MP3;

class FlacToMP3
{
    private $dir;
    private $files;
    private $flac;
    private $mode = 0;
    private $verbose = false;

    /** @var FileViewer $file_viewer */
    private $file_viewer;

    /** @var FileManager $file_manager */
    private $file_manager;

    /** @var Forker $forker */
    private $forker;

    /** @var getID3 $tag_reader */
    private $tag_reader;

    public function __construct(string $dir)
    {
        $this->dir = $dir;
        $this->file_viewer = new FileViewer($dir);
        $this->file_manager = new FileManager($dir);
        $this->forker = new Forker();
        $this->flac = new FLAC($dir);
        $this->mp3 = new MP3($dir);
        $this->tag_reader = new getID3();

    }

    public function run()
    {
        $this->file_viewer->scanDirectory();
        $flac_extension = $this->flac->getExtension();
        $this->files =  $this->file_viewer->getFilesByFormat($flac_extension);

        if(($this->files === null)||(count($this->files) == 0)) {
            throw new Exception("FLAC files not found in [".$this->dir."]");
        }

        $wav_dir = $this->file_manager->createWavDirectory();
        $mp3_dir = $this->file_manager->createMP3Directory();
        $this->flac->setOutputDirectory($wav_dir);

        foreach($this->files as $file) {
            $path = $this->dir."/".$file;
            if(! file_exists($path)) {
                printf("File %s not found\n", $path);
            }

            if($this->mode == 0) {
                $this->forker->fork([$this,'renderFile'], [$path, $wav_dir, $mp3_dir]);
            } else {
                $this->renderFile($path, $wav_dir, $mp3_dir);
            }
        }

        if($this->mode == 0) {
            $this->forker->waitpid();
        }

        $this->file_manager->removeTemporary();
    }

    public function renderFile(string $path, string $wav_dir, string $mp3_dir)
    {
        $fileinfo = pathinfo($path);
        $wav_file = $wav_dir."/".$fileinfo['filename'].".wav";
        $mp3_file = $mp3_dir."/".$fileinfo['filename'].".mp3";

        if (! $this->flac->decode($path, $wav_file)) {
            throw new Exception("Failed to decode [$path] file");
        }

        $tag_info = $this->tag_reader->analyze($path);
        $track_params = $this->getSongTags($tag_info['tags']['vorbiscomment']);

        $this->mp3->encode($wav_file, $mp3_file, $track_params, $this->verbose, false );
    }

    private function getSongTags(array $data = [])
    {
        $params = [];

        $params['date'] = $data['date'][0] ?? null;
        $params['genre'] = $data['genre'][0] ?? null;
        $params['artist'] = $data['artist'][0] ?? null;
        $params['name'] =  $data['album'][0] ?? null;
        $params['track_number'] = (int) $data['tracknumber'][0] ?? null;
        $params['title'] = $data['title'][0] ?? null;

        return $params;
    }
}