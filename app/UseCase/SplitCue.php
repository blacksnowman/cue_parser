<?php
/**
 * Created by PhpStorm.
 * User: amikhaylov
 * Date: 14.06.18
 * Time: 19:25
 */

namespace App\UseCase;

use App\Models\AlbumConverter;
use App\Models\Encoders\MP3;
use App\Models\FileManager;
use Exception;
use App\Models\FileViewer;
use App\Models\CueParser;
use App\Models\CueSplitter;
use App\Models\cpTimer;

class SplitCue
{
    private $dir;

    private $albums = [];

    /** @var cpTimer $timer */
    private $timer;

    /** @var CueSplitter $cue_splitter */
    private $cue_splitter;

    /** @var CueParser $cue_parser */
    private $cue_parser;

    /** @var FileManager $file_manager */
    private $file_manager;

    /** @var FileViewer $file_viewer */
    private $file_viewer;

    public function __construct(string $dir = null)
    {
        if($dir === null) {
            throw new Exception("Directory is not defined");
        }

        $this->dir = $dir;
        $this->file_viewer = new FileViewer($dir);
        $this->file_manager = new FileManager($dir);
        $this->file_viewer->scanDirectory();
        $this->timer = new cpTimer("micro");
    }

    public function splitCUEtoMP3()
    {
        $this->splitCUE();
        $this->timer->start();

        $mp3_encoder = new MP3();
        $converter = new AlbumConverter($this->file_manager, $mp3_encoder);
        $converter->convertAllAlbumsToMP3($this->albums);

        $this->file_manager->removeTemporary();

        $this->timer->stop("Converting to MP3 time");
    }

    public function getTimers()
    {
        $this->timer->printAllMarkers();
    }

    public function getAlbums()
    {
        $this->cue_parser = new CueParser($this->dir);
        $cue_files = $this->file_viewer->getCueFiles();
        $this->cue_parser->setCueFiles($cue_files);
        $this->albums = $this->cue_parser->parseAllFiles();

        return $this->albums;
    }

    public function splitCUE()
    {
        $this->timer->start();
        $this->cue_parser = new CueParser($this->dir);
        $cue_files = $this->file_viewer->getCueFiles();

        $this->cue_parser->setCueFiles($cue_files);

        $this->albums = $this->cue_parser->parseAllFiles();
        $this->timer->stop("Parsing time");

        $this->cue_splitter = new CueSplitter($this->dir, $this->albums);

        $this->timer->start();
        $this->cue_splitter->splitAllCue();
        $this->timer->stop("Splitting time");
    }
}