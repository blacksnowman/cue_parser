#!/usr/bin/php

<?php

require __DIR__ . '/vendor/autoload.php';

use App\UseCase\SplitCue;
use App\UseCase\FlacToMP3;
use App\Helpers\ConfigReader;

$cmd = $argv[1] ?? null;
$dir = $argv[2] ?? null;

switch($cmd) {
    case "split2mp3":
        $splitter = new SplitCue($dir);
        $splitter->splitCUEtoMP3();
        $splitter->getTimers();
    break;

    case "splitcue":
        $splitter = new SplitCue($dir);
        $splitter->splitCUE();
        $splitter->getTimers();
        break;

    case "getalbums":
        $splitter = new SplitCue($dir);
        $albums = $splitter->getAlbums();
        print_r($albums);
        break;

    case "flac2mp3":
        $use_case = new FlacToMP3($dir);
        $use_case->run();
        break;

    case "config":
        $config_reader = ConfigReader::getInstance();
        $conf = $config_reader->getConfig();

        $needle = $config_reader->getElement($dir);

        break;

    default:
        printf("Invalid command: %s\n", $cmd);
        break;
}
?>